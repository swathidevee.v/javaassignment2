package org.java;

import java.util.ArrayList;
import java.util.List;

public class Collection01 {
	 public static void main(String[] args) {
		 
		  List<String> list_Strings = new ArrayList<String>();
		  list_Strings.add("Red");
		  list_Strings.add("Green");
		  list_Strings.add("Orange");
		  list_Strings.add("White");
		  list_Strings.add("Black");
		  // Search the value Red
		    if (list_Strings.contains("Red")) {
		    System.out.println("Found the element");
		    } else {
		    System.out.println("There is no such element");
		    }
		    
		 // Retrive the first and third element
		    String element = list_Strings.get(0);
		    System.out.println("First element: "+element);
		    element = list_Strings.get(2);
		    System.out.println("Third element: "+element);    
		    
		    
		    
		    
		    
		    
}}
